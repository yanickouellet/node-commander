module.exports = (app) ->
  class HomeController
    @index: (req, res) ->
      res.render 'home/index.jade'
