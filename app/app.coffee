express = require 'express'
http = require 'http'
path = require 'path'

app = express()

app.set 'env', process.env.NODE_ENV
app.set 'path.root', rootPath = __dirname + '/..'
app.set 'path.app', appPath = __dirname
app.set 'path.config', appPath + '/config'
app.set 'path.services', appPath + '/services'
app.set 'path.assets', appPath + '/assets'
app.set 'path.controllers', appPath + '/controllers'
app.set 'path.models', appPath + '/models'
app.set 'path.views', appPath + '/views'
app.set 'path.public', rootPath + '/public'

app.set 'views', appPath + '/views'
app.set 'view engine', 'jade'
app.set 'view options', layout: false

require(app.get('path.config') + '/config.coffee')(app)

app.use express.static(app.get('path.public'))

Sequelize = require 'sequelize'
sequelize = new Sequelize app.get('db.database'),
                          app.get('db.user'),
                          app.get('db.password'),
                          dialect: 'postgres',
                          port: 5432,
                          logging: false

#sequelize.authenticate().complete (err) -> throw err if err

autoload = require(app.get('path.config') + '/autoload')(app)
for component in ['controllers', 'models', 'services']
  autoload app.get('path.' + component), component

require(app.get('path.config') + '/routes')(app)

unless process.env.NODE_CLI
  port = process.env.PORT || 3000
  app.set 'port', port
  app.locals.server = http.createServer(app).listen port, ->
    console.log "Listening on port #{port} in #{app.get('env')} mode"

module.exports = app
