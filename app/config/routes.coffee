module.exports = (app) ->
  {HomeController} = app.locals.controllers

  app.route '/'
  .get HomeController.index
