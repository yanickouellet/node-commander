fs = require 'fs'
path = require 'path'
_ = require 'underscore'
_.str = require 'underscore.string'
_.mixin _.str.exports()

to_module_name = (filename, lower_camelized = false, short_cased = false) ->
  if short_cased
    name = _(filename).chain().words('.').first().words('_').first().camelize()
  else name = _(filename).chain().words('.').first().camelize()
  if lower_camelized then name.value() else name = name.capitalize().value()

autoload = (app, dir, namespace, lower_camelized = false, short_cased = false) ->
  return unless fs.existsSync dir

  for filename in fs.readdirSync(dir)
    pathname = path.join dir, filename
    if fs.lstatSync(pathname).isDirectory()
      autoload app, pathname, namespace, lower_camelized, short_cased
    else
      loaded_module = require(pathname)?(app)
      module_name = to_module_name filename, lower_camelized, short_cased
      app.locals[namespace][module_name] = loaded_module

module.exports = (app) ->
  (dir, namespace, lower_camelized = false, short_cased = false) ->
    app.locals[namespace] = {} unless app.locals[namespace]
    autoload app, dir, namespace, lower_camelized, short_cased
