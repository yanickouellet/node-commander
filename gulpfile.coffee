gulp = require 'gulp'
fs = require 'fs'

process.env.NODE_ENV = true
process.env.NODE_ENV ?= 'development'

gulp.task 'default', ->
  gulp.start('compile')

fs.readdirSync('./gulp').forEach (file) ->
  task = file.replace('.coffee', '')
  gulp.task task, ->
    require('./gulp/' + task)(gulp)
