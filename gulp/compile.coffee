sass = require 'gulp-sass'
coffee = require 'gulp-coffee'
concat = require 'gulp-concat'
browserify = require 'browserify'
source = require 'vinyl-source-stream'

module.exports = (gulp) ->
  gulp.src ['bower_components/foundation/css/*.css']
    .pipe concat('foundation.css')
    .pipe gulp.dest('public/css')

  gulp.src 'app/assets/sass/*.scss'
    # sourceComments and sourceMap are needed for now: https://github.com/sass/node-sass/issues/337#issuecomment-45748757
    .pipe sass sourceComments: 'map', sourceMap: 'sass'
    .pipe concat('app.css')
    .pipe gulp.dest('public/css')

  browserify entries: ['./app/assets/coffee/app.coffee'], extensions: ['.coffee']
    .bundle()
    .pipe source('app.js')
    .pipe gulp.dest('public/js')
